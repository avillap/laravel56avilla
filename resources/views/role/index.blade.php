@extends('layouts.app')
 

@section('content')
<div class="container">

<h1>Lista de Usuarios</h1>

<table class="table table-bordered">
     <tr>
        <th>Nombre</th>
        <th>Email</th>
        <th>Role</th>
        <th>Prestamos</th>
     </tr>
 @foreach ($users as $user)
 <tr>
     <td>{{ $user->name }}</td>
     <td>{{ $user->email }}</td>
     <td>{{ $user->role->name }}</td>
    <td>
        <ul></ul>
            @foreach ($user->borrows as $borrow)
            <li>
                {{ $borrow->book->name }}
            </li>
            @endforeach
        </ul>
    </td>
 </tr>
 @endforeach
 </table>
</div>
@endsection
 