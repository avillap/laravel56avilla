AGENDA DE EVENTOS

- Usuario admin y usuario normal
- Los usuarios crean eventos
- Todos ven toda la agenda
- Los eventos los editan los administradores o el propio propietario
- Un evento puede afectar a varios usuarios como participantes
- Un evento puede afectar a varios grupos (grupos de alumnos).
- Los administradores crean los grupos

