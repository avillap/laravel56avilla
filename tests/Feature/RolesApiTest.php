<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RolesApiTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */


    /** @test */
    public function testApiRoles()
    {

        $this->get('/api/roles')
            ->assertStatus(200)
            ->assertJsonStructure([['id', 'name', 'created_at', 'updated_at']]);
    }
}