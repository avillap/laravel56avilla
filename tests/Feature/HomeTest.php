<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HomeTest extends TestCase
{
    /** @test */
    public function show_home()
    {
        $this->get('/')
            ->assertStatus(200)
            ->assertSee('Laravel');
    }
}
