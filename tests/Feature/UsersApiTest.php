<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UsersApiTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function testapiusers()
    {
        $this->get('/api/users')
            ->assertStatus(200)
            ->assertJsonStructure([['id', 'name', 'email', 'password', 'remember_token', 'created_at', 'updated_at', 'id_role']]);
            ->assertExactJson([
               {
                   "id"=>1,
                   "name"=>"ejem1",
                   "email"=>"ejem1@gmail.com",
                   "password"=>"123",
                   "remember_token"=>null,
                   "created_at"=>null,
                   "updated_at"=>null,
                   "id_role"=>1
               },
               {
                   "id"=>2,
                   "name"=>"ejem2",
                   "email"=>"ejem2@gmail.com",
                   "password"=>"123",
                   "remember_token"=>null,
                   "created_at"=>null,
                   "updated_at"=>null,
                   "id_role"=>2
               }
            ]);
    }
}