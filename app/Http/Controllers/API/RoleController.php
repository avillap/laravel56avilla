<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use \App\Role;

class RoleController extends Controller
{
    public function index()
    {
        return \App\Role::all();
    }
}