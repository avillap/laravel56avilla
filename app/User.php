 <?php
 
 namespace App;
 
 use Illuminate\Notifications\Notifiable;
 use Illuminate\Foundation\Auth\User as Authenticatable;
 
 class User extends Authenticatable
 {
     use Notifiable;
 
     /**
      * The attributes that are mass assignable.
      *
      * @var array
      */
     protected $fillable = [
        'name', 'email', 'password', 'role_id',
     ];
 
     public function role()
     {
        return $this->belongsTo('App\Role', 'role_id');
     }
     public function borrows() {
        return $this->hasMany('App\Borrow');
     }
 
 }