<?php
 
 use Illuminate\Database\Seeder;
 
 class UsersTableSeeder extends Seeder
 {
     /**
      * Run the database seeds.
      *
      * @return void
      */
     public function run()
     {
         DB::table('users')->insert([
             'name' => 'Bibliotecario',
             'email' => 'bibliotecario@gmail.com',
             'password' => '123',
             'role_id' => 1,
         ]);
         DB::table('users')->insert([
             'name' => 'Lector',
             'email' => 'lector@gmail.com',
             'password' => '123',
             'role_id' => 2,
        ]);
        DB::table('users')->insert([
            'name' => 'Lector',
            'email' => 'admin@gmail.com',
            'password' => 'secret',
            'role_id' => 1,
        ]);
        DB::table('users')->insert([
            'name' => 'Lector',
            'email' => 'user@gmail.com',
            'password' => 'secret',
            'role_id' => 1,
         ]);
     }
 }